#include <iostream>
#include <functional>
#include <algorithm>
#include <vector>

using namespace std;   // <-- For convenience.
using namespace std::placeholders;   // _1, _2, _3, etc.


// A 'Cosa' class
class Cosa {
    private:
        int id;

    public:
        // Default and one-param constructor
        Cosa(int i = 0) : id(i) {}	
        
        // const member function
        void print() const {
            cout << "Cosa " << id << ".\n";
        }
        
        // Write to a supplied ostream
        void to_stream(ostream& out) const {
            out << "Cosa" << id << " written to stream.\n";
        }

        // const member function with one argument
        void print_1_arg(int j) const {
            cout << "Cosa " << id << " with argument " << j << ".\n";
        }

        // const member function with two arguments
        void print_2_arg(int j, int k) const {
            cout << "Cosa " << id << " with arguments " << j << ',' << k << ".\n";
        }

        // Modifying function with no arguments
        void update() {
            id++; 
            cout << "Cosa updated to " << id << "\n";
        }

        // Modifying function with one argument
        void set_value(int i) {
            id = i;
            cout << "Cosa value set to " << i << "\n";
        }

        // Accessor function
        int get_value() const {
            return id;
        }
};



// Overload of operator<< 
ostream& operator<<( ostream& out, const Cosa& c ) {
    out << "Cosa: " << c.get_value();
    return out;
}



// Non-member functions similar to the members above.
// These functions DO NOT modify the parameters.
void print_Cosa(Cosa c) {
    c.print();
    return;
}

void print_Cosa_const_ref(const Cosa& c) {
    c.print();
    return;
}

void print_int_Cosa(int i, Cosa c) {
    cout << "print_int_Cosa: " << i << ", " << c << ".\n";
    return;
}

void print_Cosa_int(Cosa c, int i) {
    cout << "print_Cosa_int: " << c << ", " << i << ".\n";
    return;
}

void print_Cosa_int_int(Cosa c, int i, int j) {
    cout << "print_Cosa_int_int: " << c << ", " << i << ", " << j << ".\n";
    return;
}

// Non-member functions similar to the members above.
// These functions DO modify the parameters.
void update_Cosa(Cosa& c){
    c.update();
    return;
}

void set_Cosa(Cosa& c, int i){
    c.set_value(i);
    return;
}



int main(){

    int n1 = 21;
    int n2 = 32;
    Cosa c1(1), c2(2), c3(3);

    typedef vector<Cosa> Cosa_list_t;
    Cosa_list_t listOfCosas = {c1, c2, c3};


    // We can use algorithms (e.g., `for_each`) to apply non-member and
    // member functions to each Object in a container.
    
    // Non-members that DO NOT modify contents
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( print_Cosa, _1 ) );
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( print_Cosa_int_int, _1 , n1, n2 ) );

    // Non-members that DO modify contents
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( update_Cosa, _1 ) );
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( set_Cosa, _1, n1 ) );

    // Members that DO NOT modify contents
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( &Cosa::print, _1 ) );
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( &Cosa::to_stream, _1, ref(cout) ) );
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( &Cosa::print_1_arg, _1, n2 ) );

    // Members that DO modify contents
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( &Cosa::update, _1 ) );
    for_each( listOfCosas.begin(), listOfCosas.end(), 
              bind( &Cosa::set_value, _1, n2/2 ) );


    
    // OK! But what about containers of pointers?
    // The thing is `bind` is way smarter than our attempt at
    // emulating `bind2nd`. It is "smart enough" to automatically
    // adjust its behavior without changing the calls.

    typedef vector<Cosa*> Cosa_ptr_list_t;
    Cosa_ptr_list_t listOfCosaPtrs = {&c1, &c2, &c3};

    for_each( listOfCosaPtrs.begin(), listOfCosaPtrs.end(), 
              bind( &Cosa::print, _1 ) );
    for_each( listOfCosaPtrs.begin(), listOfCosaPtrs.end(), 
              bind( &Cosa::to_stream, _1, ref(cout) ) );
    for_each( listOfCosaPtrs.begin(), listOfCosaPtrs.end(), 
              bind( &Cosa::print_1_arg, _1, n2 ) );

    for_each( listOfCosaPtrs.begin(), listOfCosaPtrs.end(), 
              bind( &Cosa::update, _1 ) );
    // Finish this by copy/pasting lines 164 and 145.


    return 0;
}
