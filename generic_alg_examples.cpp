// Simple generic functions and examples of calls to 
// STL algorithms

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <string>


using std::cout;
using std::vector;
using std::list;
using std::string;


// Generic function that displays elements in a given range
template<typename InputIterator>
void print( InputIterator a, InputIterator b ){
    while ( a != b )
        std::cout << *a++ << " ";
    return;
}


int main(){

    // Vector with arbitrary entries
    vector<int> v = {1, 4, 7, 3, 9};

    // Copy of the vector presented as a list. An inserter is used 
    // because the list is initially empty.
    list<int> l;
    copy(v.begin(),v.end(),back_inserter(l));

    // Verifying the data is there
    cout << "In v: ";
    print(v.begin(),v.end());
    cout << "\nIn l: ";
    print(l.begin(),l.end());

    // Reverse copy of the vector presented as a list. A regular 
    // iterator is used because the list can hold all new values.
    list<int> l2(l);
    copy(l.rbegin(),l.rend(),l2.begin());
    cout << "\nIn l2: ";
    print(l2.begin(),l2.end());

    // Another reverse copy... 
    copy(l.begin(),l.end(),v.rbegin());
    cout << "\nAfter reversing v: ";
    print(v.begin(),v.end());


    /** 
     *  While it is true that creating generic functions is 
     *  `not that hard`, we could [and should] do better. 
     *
     *  Imagine a scenario where we want to use a separator other
     *  than a space in the display. Depending on certain conditions
     *  (our mood, for example), we could use comma, a end of line
     *  character, or maybe even let a user choose the separator.
     *
     *  With this particular setup, adding the functionality
     *  described above amounts to
     *  + Modifying the signature of `print` so that it takes
     *    a third parameter with default value " ".
     *  + Replacing the line 
     *          std::cout << *a++ << " ";
     *    so that it displays the separator instead.
     *
     *  Those are simple changes and, if anything, the most annoying
     *  part about them is the need to scroll up/down to reach
     *  the definition of the function.
     *
     *  This is where lambda functions come into play...
     */

    cout << "\n";
    return 0;
}
