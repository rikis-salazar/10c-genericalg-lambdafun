#include <iostream>
#include <functional>
#include <algorithm>
#include <vector>

using namespace std;   // <-- For convenience.
using namespace std::placeholders;   // _1, _2, _3, etc.


// A 'Cosa' class
class Cosa {
    private:
        int id;

    public:
        // Default and one-param constructor
        Cosa(int i = 0) : id(i) {}	
        
        // const member function
        void print() const {
            cout << "Cosa " << id << ".\n";
        }
        
        // Write to a supplied ostream
        void to_stream(ostream& out) const {
            out << "Cosa" << id << " written to stream.\n";
        }

        // const member function with one argument
        void print_1_arg(int j) const {
            cout << "Cosa " << id << " with argument " << j << ".\n";
        }

        // const member function with two arguments
        void print_2_arg(int j, int k) const {
            cout << "Cosa " << id << " with arguments " << j << ',' << k << ".\n";
        }

        // Modifying function with no arguments
        void update() {
            id++; 
            cout << "Cosa updated to " << id << "\n";
        }

        // Modifying function with one argument
        void set_value(int i) {
            id = i;
            cout << "Cosa value set to " << i << "\n";
        }

        // Accessor function
        int get_value() const {
            return id;
        }
};



// Overload of operator<< 
ostream& operator<<( ostream& out, const Cosa& c ) {
    out << "Cosa: " << c.get_value();
    return out;
}



// Non-member functions similar to the members above.
// These functions DO NOT modify the parameters.
void print_Cosa(Cosa c) {
    c.print();
    return;
}

void print_Cosa_const_ref(const Cosa& c) {
    c.print();
    return;
}

void print_int_Cosa(int i, Cosa c) {
    cout << "print_int_Cosa: " << i << ", " << c << ".\n";
    return;
}

void print_Cosa_int(Cosa c, int i) {
    cout << "print_Cosa_int: " << c << ", " << i << ".\n";
    return;
}

void print_Cosa_int_int(Cosa c, int i, int j) {
    cout << "print_Cosa_int_int: " << c << ", " << i << ", " << j << ".\n";
    return;
}

// Non-member functions similar to the members above.
// These functions DO modify the parameters.
void update_Cosa(Cosa& c){
    c.update();
    return;
}

void set_Cosa(Cosa& c, int i){
    c.set_value(i);
    return;
}



int main(){

    int n1 = 10;
    int n2 = 20;
    int n3 = 30;
    Cosa c1(1);

    // Non member function calls involving a Cosa object are just like the
    // examples described in `bind.cpp`. The only difference this time is
    // that a `Cosa` object can be a bound argument or a call argument.
    bind( print_Cosa, c1 )();
    bind( print_Cosa_const_ref, c1 )();
    bind( print_Cosa_int, c1, n1 )();

    bind( print_Cosa, _1 )(c1);
    bind( print_Cosa_const_ref, _1 )(c1);
    bind( print_Cosa_int, _1, _2 )(c1,n1);


    
    // We can also call functions that modify the supplied parameters,
    // but we need to determine whether or not we are actually modifying
    // the parameters instead of copies of them.
    cout << "\nBefore: " << c1 << ".\n";
    bind( update_Cosa, c1 )();          // modifies copy of c1
    cout << "After: " << c1 << ".\n";

    cout << "\nBefore: " << c1 << ".\n";
    bind( update_Cosa, ref(c1) )();     // modifies original c1
    cout << "After: " << c1 << ".\n";

    cout << "\nBefore: " << c1 << ".\n";
    bind( update_Cosa, _1 )(c1);        // modifies original c1
    cout << "After: " << c1 << ".\n";


    cout << "\nBefore: " << c1 << ".\n";
    bind( set_Cosa, c1, n3 )();         // modifies copy of c1
    cout << "After: " << c1 << ".\n";

    cout << "\nBefore: " << c1 << ".\n";
    bind( set_Cosa, ref(c1), n2 )();    // modifies original c1
    cout << "After: " << c1 << ".\n";

    cout << "\nBefore: " << c1 << ".\n";
    bind( set_Cosa, _1, n1 )(c1);       // modifies original c1
    cout << "After: " << c1 << ".\n\n";

    

    // We can also call member functions of Cosa objects. `bind` is
    // "smart enough" (c.f. `bind1st`) to 1) automatically figure out
    // that a pointer-to-a-member-function is involved, and 2) set up
    // the correct call to it. However, the compiler still needs help.
    // Unlike with an ordinary function, this time we DO need the
    // ampersand [&] to explicitly request the memory address of the
    // member function.
    bind( &Cosa::print, c1 )();
    bind( &Cosa::print, _1 )(c1);
    bind( &Cosa::to_stream, _1, ref(cout) )(c1);
    bind( &Cosa::print_1_arg, _1, _2 )(c1, n2);
    bind( &Cosa::print_2_arg, _1, _2, _3 )(c1, n2, n3);

    cout << "\n";
    bind( &Cosa::update, _1 )(c1);           // modifies original c1
    bind( &Cosa::set_value, _1, n1 )(c1);    // modifies original c1
    bind( &Cosa::update, c1 )();             // modifies copy of c1
    cout << "Just kidding! " << c1 << ".\n";
    bind( &Cosa::update, ref(c1) )();        // modifies original c1
    cout << "Not kidding! " << c1 << ".\n";

    return 0;
}
