# Lambda functions (a.k.a. closures)

One of the new features of C++11 is the ability to create **lambda
functions**.
A lambda function is a function that you can write _inline_, that is,
within any of your functions. Think about it, how many times you have found
yourself scrolling up/down to the top/bottom of your source file just to
declare and/or define a simple function that is one or two lines long.
If the answer is more than once, then lambda functions are just for you
(however, I must warn you that the syntax is quite ugly).

With the addition of the lambda functionality, creating quick functions
has become much easier. In the context of our example above, this means
that you can write the function right there where it is needed. Moreover,
it has the ability to _capture_ objects that belong to the same scope.
If you want the same functionality with regular functions, you have no
choice but to add extra parameters.

## Syntax

According to
[cppreference.com](http://en.cppreference.com/w/cpp/language/lambda)
a lambda function has to have _more or less_ the form

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
[ capture-list ] ( params ) exception attribute -> ret { body }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

I write _more or less_ because this is a simplified version of the
most general case. Ugly, isn't it? Do not worry, in practice lambda
functions are not that ugly in person.

Consider the following example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <iostream>

int main(){
    auto lambda_fun = []() { std::cout << "Hello, Lambda-World!\n"; };
    lambda_fun();

    return 0;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

See? They are not that ugly after all. Are they?

In this case we are creating a lambda function and _storing it_
(kinda like a function pointer) into `lambda_fun`. Right after we 
proceed to use this function, and the result is the expected 
_Hello-world-like_ program.

But wait! What about the **capture**, the **parameters**, the
**return type**, and the like? Take a look at `fun_with_lambda.cpp`
within this repository. I will try to explain with more examples
when, and how to _add these guys_ to the mix.
