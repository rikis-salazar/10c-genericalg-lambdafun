
// Simple generic functions and examples of calls to 
// STL algorithms

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <string>
#include <functional>


using std::function;
using std::bind2nd;
using std::cin;
using std::cout;
using std::vector;
using std::list;
using std::string;


int main(){

    // Vector with arbitrary entries
    vector<int> v = {1, 4, 7, 3, 9};

    // Copy of the vector presented as a list. An inserter is used 
    // because the list is initially empty.
    list<int> l;
    copy(v.begin(),v.end(),back_inserter(l));



    // Verifying the data is there
    // This time, we rely on the STL algorithm for_each

    auto print_int = [](int a){ cout << a << " "; };

    cout << "In v: ";
    for_each(v.begin(),v.end(),print_int);



    // Not bad at all! But how about this statement to display the
    // contents of the list
    cout << "\nIn l: ";
    for_each(l.begin(),l.end(),[](int a){ cout << a << "->"; });


    // Reverse copy of the vector presented as a list. A regular 
    // iterator is used because the list can hold all new values.
    list<int> l2(l);
    copy(l.rbegin(),l.rend(),l2.begin());


    // Ok. How about we let the user choose the separator.
    cout << "\nEnter a separator: ";
    string input = " ";
    cin >> input;

    /**
     *  We have the right idea here. The problem is that `for_each`
     *  expects a function object [or  function pointer], that 
     *  takes only one argument. And in this case, our lambda 
     *  function takes two arguments...
     *
     *  We could try to `bind` one of them... but it is actually
     *  a little tricky. Instead we should 'capture' the input
     *  string and use it within our lambda function.
     */

    // Let us try again...
    cout << "\nIn l2: ";
    for_each(l2.begin(),l2.end(),
             [=](int a){ cout << a << input; }); // How cool is this?
    //       ^^^    Indicates the we want to capture 'by value'.


    
    // We can also capture by reference...
    cout << "\nIn v [reverse]: ";
    for_each(v.rbegin(),v.rend(),
             [&](int a){ cout << a << input; input += "-" ; }); 
    cout << "\n";

    // By value again
    cout << "\nIn v: ";
    for_each(v.begin(),v.end(),
             [=](int a){ cout << a << input; }); 
    cout << "\n";


    // And out of curiosity ...
    /**
    cout << "\nIn v: ";
    for_each(v.begin(),v.end(),
             [=](int a){ cout << a << input; input += "-" ; }); 
    cout << "\n";
    */


    // So, what is this binding thingy??? Essentially it amounts to reducing the
    // number of parameters from 2 to 1, either by 'fixing' the first, or the
    // second value.
    //
    // However, they are a little tricky.
    //
    // auto fun = [](int a, string s){ cout << a << s; };  // NOT QUITE!
    //
    // void (fun*)(int,string) = [](int a, string s){ cout << a << s; }; // NOPE!
    //
    // void (fun*)(int,string);
    // fun = [](int a, string s){ cout << a << s; }; // NO-NO!
    //
    /**
    cout << "\nLas time... In v: ";
    for_each(v.begin(),v.end(), bind2nd(fun,"--")); 
    */

    
    // The combinations above fail for one reason or another...
    // However, we can always 'add an extra layer' to our objects.
    function<void(int,string)> fun = [](int a, string s){ cout << a << s; };
    cout << "\nLas time... In v: ";
    for_each(v.begin(),v.end(), bind2nd(fun,"--")); 
    /**
    */

    cout << "\n";
    return 0;
}
