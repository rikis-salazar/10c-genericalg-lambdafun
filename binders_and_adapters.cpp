// ***********************************************
//      W   A   R   N   I   N   G   !   !   !
//
// This is a non-working example. In addition, the functions discussed here
// are now deprecated and are set to be removed from c++ once the c++17
// standards are implemented.
//
// ***********************************************



/**
    In this example we learn how binders and adapters work 'under the hood'. 
   
    Recall that in our previous example we turned a two-parameter
    function into a one-parameter function via the `bind2nd` function.
   
      function<void(type1,type2)> twoParamFun = // Some Lambda Function ; 
      auto oneParamFun = bind2nd( twoParamFun, fixedType2Value );
   
    We then were able to use this function in combination with a
    generic algorithm (e.g. `for_each`).
   
    In this example we look at a similar situation. Consider the code:
*/

#include <list>
#include <algorithm>
#include <functional>

using namespace std;  // For simplicity

const double PI = 3.1416;
void twoParamFun(int, double);  

// ...

{ // Denotes some local scope (e.g., main)

    list<int> l; 
    // Populate list ...

    // GOAL STATEMENT
    for_each( l.begin(), l.end(), bind2nd( ptr_fun(twoParamFun), PI ) );

    // This is the same principle we used. However, we employed a 
    // hybrid approach: function<> + bind2nd.
    //   
    // Here `ptr_fun` is being used instead of the function<> template.
    //
    // Note:
    //
    //    Both functions `ptr_fun` and `bind2nd` are now deprecated.
    //    Use `function<>` instead of `ptr_fun`.
    //    Use `bind` instead of either `bind1st` or `bind2nd`.

}
/** 
    SPOILER ALERT!

    The statement above is nothing but an equivalent version of the loop

    for ( list<int>::iterator iter = l.begin() ; iter != l.end(); ++iter )
        twoParamFun(*iter, PI);
 */



/** 
    Basically, we are dealing with a couple of wrappers. The first one
    takes a function pointer and returns a function object with the
    'correct overload' of `operator()`.

    If we really needed to, we could code this wrapper ourselves. Luckily
    we do not need to, as the standard library has a template class (struct)
    that holds 'typedefs' for binary functions.
 */
template<typenane ARG1, typename ARG2, typename RESULT>
struct binary_function{
    // No big deal here, just a set of standard names that everyone can use. 
    typedef ARG1    first_argument_type;
    typedef ARG2    second_argument_type;
    typedef RESULT  result_type;
};



/**
    With this struct, the first step is now almost trivial although maybe 
    a little hard to visualize due to the syntax associated to function
    pointers.
 */
template<typenane ARG1, typename ARG2, typename RESULT>
class pointer_to_binary_function : public binary_function<ARG1,ARG2,RESULT> {
    private:
        // We just need a function pointer... But function pointer declarations
        // are ugly. Aren't they?
        RESULT (*function_ptr)(ARG1,ARG2); 

    public:
        // We just need to initialize the only field...
        explicit pointer_to_binary_function( RESULT (*f_ptr)(ARG1,ARG2) ) 
            : function_ptr( f_ptr ) {}

        // and overload operator()
        RESULT operator()(ARG1 a, ARG2 b) const {
            return function_ptr(a,b);
        }
};



/**
    Next, we need one more template function to deal with the inner
    part of our GOAL STATEMENT.
 */
template<typename ARG1, typename ARG2, typename RESULT>
inline                                          // write code in place
pointer_to_binary_function<ARG1,ARG2,RESULT>    // return type
ptr_fun( RESULT (*fun)(ARG1,ARG2) ) {           // inner part of GOAL STATEMENT
    return pointer_to_binary_function<ARG1,ARG2,RESULT>(fun);
}



/** 
    That was easy! Wasn't it?
    The TRICK here is to use a function template [ptr_fun] to create and
    return an instance of a template function. Notice that, because it is
    a function instead of a class, the compiler does not need to explicitly
    know the template parameters.

    Before going on with our discussion, let us pause to analyze the current
    state if our GOAL STATEMENT.
 */

void twoParamFun(int, double);  
// ...

// GOAL ...
    for_each( l.begin(), l.end(), bind2nd( ptr_fun(twoParamFun), PI ) );

// For the inner call ptr_fun( twoParamFun )
// The template parameters are 
//     ARG1 = int
//     ARG2 = double
//     RESULT = void
// the explicit parameter is
//     fun = twoParamFun
// and the call results in a template instantiation that looks like this
pointer_to_binary_function<int,double,void>
ptr_fun( void (*fun)(int,double) ) {
    return pointer_to_binary_function<int,double,void>(fun);
}

// In turn, when this line is executed, creates and returns an object
// whose class name is pointer_to_binary_function<int,double,void>
// and it is instantiated as
template<typenane int, typename double, typename void>
class pointer_to_binary_function : public binary_function<int,double,void> {
    private:
        void (*function_ptr)(int,double); 

    public:
        explicit pointer_to_binary_function( void (*f_ptr)(int,double) ) 
            : function_ptr( f_ptr ) {}

        void operator()(int a, double b) const {
            return function_ptr(a,b);
        }
};
// where the field `function_ptr` has been initialized to be the address of
// `twoParamFun`. 
//
// In other words, if we use an object of this class we are actually calling
// the function `twoParamFun`. In this case, we still need to provide two 
// parameters, namely, an int and a double. 



/**
    Since we want to be able to 'fix' one of the parameters, we add yet another
    layer of template complexity to achieve this. The idea is to use another
    function object class that saves the second value, and provides an
    appropriate overload of `operator()`. This function object is known as
    a 'binder'.
 */

// Since [math] operations are typical examples of two parameter functions,
// we name the template parameter accordingly. Just keep in mind that we 
// actually expect a function object of type `binary_function`, and because
// of this, we have a standard set of names (typedefs) that we can take
// advantage of.
template<typename OPERATION>
class binder2nd {
    private:
        // We'll need a holder for a `binary_function` ...
        OPERATION oper;

        // as well as a holder for the second argument
        typename OPERATION::second_argument_type fixed_value;

    public:
        // As before we need a constructor ...
        binder2nd( const OPERATION& _op, 
                   const typename OPERATION::second_argument_type& _val ) 
            : oper(_oper), fixed_value(_val) {}

        // and the correct overload of `operator()`
        typename OPERATION::result_type        // return type
        operator()( const typename OPERATION::first_argument_type& first ) const {
            return oper(first,fixed_value);
        }
};


/**
    As before, this is not enough if we want to call our GOAL STATEMENT
    without explicitly specifying the template parameters. Fortunately,
    we know how to deal with this. Yes, you guessed it correctly: yet 
    one more layer of template complexity.
 */
template<typename OPERATION, typename T>
inline                                          // write code in place
binder2nd<OPERATION>                            // return type
bind2nd( const OPERATION& op, const T& a ) {    // outer part of GOAL STATEMENT
    return binder2nd<OPERATION>(
             op, 
             typename OPERATION::second_argument_type(a) // convert via construct
           );
}



/**
    Just like in our previous case, when `bind2nd` is called with a function
    object `op`, it will create and return a binder2nd object using the
    overload of `operator()`. This returned object now expects only one 
    parameter and knows what the fixed value for the second parameter of `op` is.

    With this setup in place we can go back to our partial analysis of
    our GOAL STATEMENT. When the `bind2nd` call is analyzed by the compiler
    it 'sees' something like this
*/
binder2nd< pointer_to_binary_function<int,double,void> >  // return type
bind2nd( const pointer_to_binary_function<int,double,void>& op, const double& a ){
    return binder2nd< pointer_to_binary_function<int,double,void> >(
             op, 
             double(a)                                    // convert via construct
           );
}


/** 
    This creates a `binder2nd` object instantiated with the supplied function
    object (from `ptr_fun(twoParamFun)`) and the supplied double (PI). The 
    result is an object whose instantiation looks like this:
  */ 
class binder2nd {
    private:
        pointer_to_binary_function<int,double,void> oper;
        double fixed_value;

    public:
        binder2nd( const pointer_to_binary_function<int,double,void>& _op, 
                   const double& _val ) 
            : oper(_oper), fixed_value(_val) {}

        void operator()( const int& first ) const {
            return oper(first,fixed_value);
        }
};

/** 
    This object itself is initialized with `PI` in the `value` field.
    and `twoParamFun` in the `oper` fieldr; the function call operator 
    simply calls `twoParamFun(a,PI)` where `a` is the supplied integer
    parameter.

    Needless, to say that this seems incredibly complex, but once the
    compiler is done with the instantiations [and optimizations], the
    end result is something completely equivalent to

    for ( list<int>::iterator iter = l.begin() ; iter != l.end(); ++iter )
        twoParamFun(*iter,3.1416);

    If you are wondering why we went through all of this given that
    both `ptr_fun` and `bind2nd` are now deprecated, the answer is that
    their replacements [`function<>` and `bind`] are even more complex; 
    but at the same time they provide greater flexibility, while still 
    using the same ideas and techniques discussed in this 'example'.
 */


