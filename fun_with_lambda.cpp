// Examples of lambda functions, their usage, and some applications.

#include <iostream>

int main(){

    // This is fun [literally]!
    auto fun = [](){ std::cout << "Hello, Lambda-World 1!\n"; };
    fun();

    // Let's do it again!
    auto fun2 = []{ std::cout << "Hello, Lambda-World 2!\n"; };
    fun2();

    // and again!
    auto fun3 = [](void){ std::cout << "Hello, Lambda-World 3!\n"; };
    fun3();

    // and again!
    auto fun4 = [](void) -> void { std::cout << "Hello, Lambda-World 4!\n"; };
    fun4();

    // and again, and again, and again.
    auto fun_n = [](int n) {std::cout << "Hello, Lambda-World " << n << "!\n"; };
    fun_n(5);
    fun_n(6);
    fun_n(7);

    // Ok, how about we add sum [sic] numbers... (got it?)
    auto addNumbers = [](int a, int b){ return a+b; };
    int x = 3;
    int y = 5;
    std::cout << "The sum of " << x << "+" << y << " is " 
              << addNumbers(x,y) << "\n";

    // Wait a minute. We still have to declare it before we use it. Don't we?
    // Actually, we don't. But...
    std::cout << "The product " << x << "*" << y << " is " 
              << [](int a, int b){ return a*b; } // Oops?! We've seen this before
              <<"\n";

    return 0;
}
