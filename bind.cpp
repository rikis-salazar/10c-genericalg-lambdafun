#include <iostream>
#include <functional>
#include <algorithm>
#include <vector>

using namespace std;   // <-- For convenience.
using namespace std::placeholders;   // _1, _2, _3, etc.



// A function with three _value_ parameters.
int add3ints(int a, int b, int c){
    int result = a + b + c ;
    cout << a << '+' << b << '+' << c << '=' << result << "\n";
    return result;
}


// A function with one _value_ parameter and two reference parameters.
void modify2ndAnd3rd(int a, int& b, int& c){
    b *= 2;
    c *= 3;
}


// A function that expects an uncopyable object
void writeToOutputStream( ostream& out, int a ){
    out << a << "\n";
    return;
}


int main(){
    int n1 = 3;
    int n2 = 1;
    int n3 = 4;

    auto f = bind(add3ints,n1,n2,n3);
    f();

    bind(add3ints,1,10,100)();

    cout << "Before: " << n1 << ", " << n2 << ", " << n3 << ".\n";
    bind(modify2ndAnd3rd,n3,n2,n1)();
    cout << "After: " << n1 << ", " << n2 << ", " << n3 << ".\n";


    cout << "Before: " << n1 << ", " << n2 << ", " << n3 << ".\n";
    bind(modify2ndAnd3rd,n3,n2,ref(n1))();
    cout << "After: " << n1 << ", " << n2 << ", " << n3 << ".\n";

    // bind(writeToOutputStream,cout,123)(); // <-- Oops!
    bind(writeToOutputStream, ref(cout) ,123)(); // OK!

 
    // Equivalent statements
    bind(add3ints, _2, n2, _1)(n3,n1);
    bind(add3ints, _1, n2, _2)(n1,n3);
    add3ints(n1,n2,n3);

    // Extreme examples
    bind(add3ints, n1, _5, _2)(1, n3, 10, 100, n2);
    bind(add3ints, _1, _1, _1)(n2);


    // Pass by reference and placeholders.
    cout << "Before: " << n1 << ", " << n2 << ", " << n3 << ".\n";
    // bind(modify2ndAnd3rd, _3, _2, _1)(10,n3,n1);   // Oops! 10 cannot change 
    // bind(modify2ndAnd3rd, _3, _2, _1)(n2,n1+n2,1); // Oops again!
    bind(modify2ndAnd3rd, _3, _2, _1)(n2, n3, n1);
    cout << "After: " << n1 << ", " << n2 << ", " << n3 << ".\n";

    bind(modify2ndAnd3rd, _1, _2, _3)(1+n1, n2, n3);  // OK! Why?
    cout << "After again: " << n1 << ", " << n2 << ", " << n3 << ".\n";


    vector<int> int_vec = {20, 21, 26, 18, 16};
    vector<int> w;
    cout << "Transforming 'int_vec'...\n";

    transform( int_vec.begin(), int_vec.end(), back_inserter(w),
              bind( add3ints, -10, -5, _1 ) );

    cout << "Modifying 'w'...\n";
    for_each( w.begin(), w.end(), 
              bind( modify2ndAnd3rd, _1, _1, _1 ) );

    for_each( w.begin(), w.end(), [](int x){ cout << x << " "; } );
    cout << "\n";

    return 0;
}


