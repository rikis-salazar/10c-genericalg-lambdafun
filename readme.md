# Generic algorithms

The following is just a quick summary of some of the most useful generic
algorithms from the **STL**, _Standard Template Library_.

Note(s):  

+ Although it might seem that these algorithms take a container
  as a parameter, it is important to realize that they actually take
  **iterators** as parameters, and that these iterators determine a 
  **range** of the form `[begin,end)`. This is an important pattern that you
  should try to follow when you write your own code.
+ Many of these algorithms also take a **function predicate** as
  one of their parameters. This makes C++ very flexible and provides
  great functionality.

As with the case of member functions, algorithms can be classified depending
on whether or not they modify the contents of the range of elements that they
receive. 


## Accessors

Another name that you might encounter elsewhere is that of **non-mutating**
algorithms.

### `find`

This one is to perform a simple search

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
InputIterator find( InputIterator begin,
                    InputIterator end,
                    const T& value );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Notice that this function does not return a `bool` value. Instead
it returns an `InputIterator` object. The idea here is that one
can continue searching for the same [or other values] after a call
to this function. E.g.:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
auto iter = find(v.begin(),v.end(),wantedValue);
while ( iter != v.end() ){
    cout << "Found it [again?]!";
    iter = find(++iter,v.end(),wantedValue); 
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Another thing that can be done here is to count the number of times
a wanted value appears in a given range of elements. However, like they
say: "There's ~~an app~~ a generic function for that!". 

### `find_if`

Similar to `find`, but it requires a predicate function object instead
of a value.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
InputIterator find( InputIterator begin,
                    InputIterator end,
                    Predicate pred );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Note(s):

+ Recall that any instance of a class that overloads the member function
  `operator()`, is known as a **function object**. If such an overload
  returns a `bool` value, then the class is known as a **predicate function**;
  instances of these class are then known as **predicates**.

For example, given a full range of elements in a container, the following
code finds the first occurrence of an odd number.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class IsOdd{
    public:
        operator()(int n) const { return ( n%2 != 0 ); }
} oddFinder;

auto iter = find_if(v.begin(),v.end(),oddFinder);
while ( iter != v.end() ){
    cout << "Found an odd number [again?]!";
    iter = find(++iter,v.end(),oddFinder); 
}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


### `count` and `count_if`

As the name suggests, they count occurrences of either a value, or a category
of values that can be extracted via a predicate function.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
size_t count( InputIterator begin,
              InputIterator end,
              const T& value );

size_t count_if( InputIterator begin,
                 InputIterator end,
                 Predicate pred );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

`size_t` is a primitive integer type that holds unsigned numbers and
it is defined to be big enough to hold the maximum size of a container.

The following code above can be used to count how many odd numbers are
there in a given container.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
int oddCount = count_if(v.begin(),v.end(),oddFinder);
cout << "v contains " << oddCount << " odd numbers.";
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## Numeric algorithms

These are mostly designed to work with containers of numbers. However, since
they also accept function objects (e.g., to replace 
`operator<(const T& small,const T& big)`), they are flexible and can be used
with other types.

### `max_element` and `min_element`

As their name imply, they are used to find the maximum/minimum elements
in a given range. The basic idea behind this algorithms is what you expect,
namely, if the range is not empty, set the current max/min to the first element;
then compare all remaining elements to the current max/min, and update
accordingly.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
ForwardIterator max_element( ForwardIterator begin,
                             ForwardIterator end );

ForwardIterator max_element( ForwardIterator begin,
                             ForwardIterator end,
                             Compare cmp );

// Similarly for min_element( ... )
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Note(s): 

+ Although finding the max/min in a range of elements does qualify
  as a _single pass_ algorithm, an `InputIterator` cannot be used here
  because the actual max/min value needs to be stored in the iterator
  that is returned by these functions.
+ The predicate `cmp` takes two parameters. Its return value should be
  `true` if the second parameter is _bigger_ than the first one [in some
  sense].

Here is a simple example

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
SomeContainer v;
// ... 
cout << "The max element in our data is ";
auto iter = max_element(v.begin(),v.end());
cout << *iter << ".";
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

And here is a _not so simple one_

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class CMP {
    public:
	bool operator() (int currentMax, int n) {
	    return ( (n % 2) == 1 ) && ( n > currentMax );
	}
};
 
SomeContainer data;
// ...
 
//auto iter = max_element(data.begin(),data.end(),CMP); // Oops!
auto iter = max_element(data.begin(),data.end(),CMP()); // OK.
if ( iter != data.end() )
  cout << "The 'maximum' is " <<  *iter << ".";
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

> Question:  
> Can the snippet above display ```The 'maximum' is 2016.```?
> If so, how?


### `accumulate`

As the name suggests, it accumulates or _adds up_ the values in a given
range. Notice that sometimes, _adding_ can have a different meaning.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
T accumulate( InputIterator begin,
              InputIterator end,
              T initialValue
              [, BinaryOperation op] ); // <-- Optional

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Note(s):

+ `BinaryOperation` should overload `operator()` with two parameters.
  The first parameter should be the accumulated value, whereas the second
  one is the value to be added to the accumulated one.

Here is a simple example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
cout << "The sum of all values in 'data' is: "
     << accumulate(data.begin(),data.end(),0) << ".";
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

What does the following snippet do?

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Mystery{
    public:
       double operator()(double sum, double x) const { 
           return x > 0 ? sum + x : sum ;
       }
} mysteryOp;

SomeContainer v;
// ...

cout << "The mystery accumulated value is: " 
     << accumulate(v.begin(),v.end(),0,misteryOp) // no parenthesis?
     << ".";
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## Copying algorithms

Copy ranges of elements from one container to another one. They can 
also be used with predicate objects.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
OutputIterator copy( InputIterator begin1,
                     InputIterator end1,
                     OutputIterator begin2 );

OutputIterator remove_copy( InputIterator begin1,
                            InputIterator end1,
                            OutputIterator begin2, 
                            const T& value );

OutputIterator remove_copy_if( InputIterator begin1,
                               InputIterator end1,
                               OutputIterator begin2, 
                               Predicate pred );

OutputIterator transform( InputIterator begin1, 
                          InputIterator end1,
                          OutputIterator begin2,
                          UnaryOperation op);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

`copy` copies the elements in `[begin1,end1)` to the output iterator given.
`remove_copy` only copies values that are different value passes as the
last parameter. `remove_copy_if` is the same as `remove_copy` except that
it doesn't copy values that satisfy the predicate [i.e., those for which
the return value is `true`]. Finally, `transform` performs the same task
as `copy` except that it applies the given operation `op` to each element
before storing the result .

Note(s):

+ There is no `copy_if`. Turns out it was _accidentally left out_.
+ Use `remove_copy_if` with a predicate that _does the opposite_ of what
  you want.
+ If you truly need a `copy_if` function, you can code an equivalent one.
  For example, you can use the following

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
  template<typename IN, typename OUT, typename PRED>
  OUT copy_if( IN begin, IN end, OUT dest, PRED p ){
      while ( begin != end ){
          if ( p(*begin) ){ 
              *dest = *begin;
              ++dest;
          }
          ++begin;
      }
      return dest;
  }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

If you are wondering what happens to the container associated to
`begin2`, the answer is that _it depends on what type of iterator
it is_.  If it is a normal iterator, then the elements being copied
replace the elements in the output container. In this case, issues
are likely to appear if there is not enough room in the output container.
On the other hand, if the iterator is an
[**insert iterator**](http://www.cplusplus.com/reference/iterator/insert_iterator/),
the elements being copied are inserted into the container.


## Sorting algorithms

There are several sorting algorithms defined in the STL library. Let us look
at some of them.

### `sort`

This algorithm sorts a range of elements. The `c++ 11` standard does not
require a specific algorithm, it only species that the sorting is completed
in approximately $n\log n$ steps. In other words, this algorithm will
favor speed over _stability_ (i.e., relative order of equal elements is
preserved after the sorting algorithm is applied).

As it is the case with other generic algorithms, `sort` provides
a way to alter the order by allowing a comparator to be passed as
a parameter.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void sort( RandomAccessIterator begin,
           RandomAccessIterator end
           [, Compare cmp] );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

To sort the elements of a vector in increasing order use the statement
`sort(v.begin(),v.end())`.

If stability is to be favored instead, the algorithm `stable_sort` can
be used. The set of parameters that it can receive is similar to the
ones described above.

### `partial_sort`

Sometimes, completely sorting a range of values is not really needed.
For example, in a typical grading scheme the lowest 2 or 3 homework grades
will not count towards the computation of a final grade. In this case
the function `partial_sort` can be used.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void partial_sort( RandomAccessIterator begin,
                   RandomAccessIterator middle,
                   RandomAccessIterator end
                   [, Compare cmp] );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

In our described scenario the snippet 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
auto v = returnVectorWithAtLeast5Values();
partial_sort(v.begin(),v.begin()+3,v.end());
cout << "Average: "  
     << accumulate(v.begin()+4,v.end(),0) / (v.size() - 3.0);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
would do the trick.

###
